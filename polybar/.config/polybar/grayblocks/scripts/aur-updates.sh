#!/bin/bash

get_total_aur_updates() { AUR_UPDATES=$(yay -Qua | wc -l); }

while true; do
  get_total_aur_updates 
  
  while (( AUR_UPDATES > 0 )); do
    if (( AUR_UPDATES == 1 )); then
      echo " $AUR_UPDATES"
    elif (( AUR_UPDATES > 1 )); then
      echo " $AUR_UPDATES"
    else
      echo " None"
    fi
    sleep 10
    get_total_aur_updates 
  done

  while (( AUR_UPDATES == 0 )); do
    echo " None"
    sleep 1800
    get_total_aur_updates 
  done
done
