#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH=$HOME/tools/nvim-linux64/bin:$PATH

source /home/mattia/.config/broot/launcher/bash/br
. "$HOME/.cargo/env"

export JAVA_HOME=/usr/lib/jvm/java-16-openjdk/bin/java
export PATH=$PATH:/usr/lib/jvm/java-16-openjdk/bin
